//
//  UserInfoCell.swift
//  Softcom-Project
//
//  Created by Asar Sunny on 31/12/2018.
//  Copyright © 2018 Sunny. All rights reserved.
//

import UIKit

enum formType:String{
    case text = "text",
        formattednumeric = "formattednumeric",
        datetime = "datetime",
        yesno = "yesno"
    
}

class UserInfoCell: UITableViewCell {

//    outlets
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var textFieldInput: UITextField!
    @IBOutlet weak var numberFieldInput: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        datePicker.isHidden = true
        textFieldInput.isHidden = true
        numberFieldInput.isHidden = true
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            
        }

        // Configure the view for the selected state
    }

    
    func configureCell(withLabel label:String, dataType type:formType.RawValue,isMandatory required:Bool, andRules rules:[String:Any]?){
        questionLabel.text = "\(label)"
        
        
        switch type {
        case formType.datetime.rawValue:
             self.questionLabel.text = label
            datePicker.isHidden = false
            textFieldInput.isHidden = true
            numberFieldInput.isHidden = true
        case formType.text.rawValue:
            datePicker.isHidden = true
            textFieldInput.isHidden = false
            numberFieldInput.isHidden = true
            
        case formType.formattednumeric.rawValue:
            datePicker.isHidden = true
            textFieldInput.isHidden = true
            numberFieldInput.isHidden = false
        default:
            datePicker.isHidden = true
            textFieldInput.isHidden = true
            numberFieldInput.isHidden = true
        }

        
    }
    
    
    
    
    
}
