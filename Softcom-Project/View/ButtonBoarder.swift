//
//  ButtonBoarder.swift
//  Softcom-Project
//
//  Created by Asar Sunny on 04/01/2019.
//  Copyright © 2019 Sunny. All rights reserved.
//

import UIKit
@IBDesignable
class ButtonBoarder: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderWidth = 2.0
          layer.borderColor = UIColor.white.cgColor
    }

}
