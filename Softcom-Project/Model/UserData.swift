//
//  UserData.swift
//  Softcom-Project
//
//  Created by Asar Sunny on 30/12/2018.
//  Copyright © 2018 Sunny. All rights reserved.
//

import Foundation


struct UserData {
//    static let instance = UserData()
    
    static var pageLabel:String?
    
    var fieldLabel:String
    var isMandatory:Bool
    var fieldType:String
    var rules: [String:Any]?
    var unique_id:String
    
    
    
}
