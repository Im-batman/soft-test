//
//  Intro.swift
//  Softcom-Project
//
//  Created by Asar Sunny on 31/12/2018.
//  Copyright © 2018 Sunny. All rights reserved.
//

import Foundation

struct Intro {
    
    var Title:String
    var ImageUrl: String
    
}
