//
//  Questions.swift
//  Softcom-Project
//
//  Created by Asar Sunny on 31/12/2018.
//  Copyright © 2018 Sunny. All rights reserved.
//

import Foundation

struct Question{
    
    static var pageLabel:String?
    
    var label:String
    var id: String
    var isMandatory:Bool
    var type: String

//    var rules:Rule?
    
    var action:String
    var target:String
    var value: String
    var otherwise:String
    var condition:String

    
}
//
//struct Rule {
//    var action:String
//    var target:String
//    var value: String
//    var otherwise:String
//    var condition:String
//
//}

