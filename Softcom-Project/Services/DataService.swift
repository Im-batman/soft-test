//
//  DataService.swift
//  Softcom-Project
//
//  Created by Asar Sunny on 30/12/2018.
//  Copyright © 2018 Sunny. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

struct DataService {
    static let instance = DataService()
    func fetchDataFromFile(completion:@escaping (_ data:JSON)->()){
        
       
        if let file = Bundle.main.url(forResource: "pet_adoption-1", withExtension: "json"),
            let data = try? Data(contentsOf: file), //get the data from the content of file
            let json = try? JSON(data: data){  //convert to json
            completion(json)
        }
    }
    

    
    
    //for parsing pages 2 and 3
    func parseData(json:JSON, Completion:@escaping ([Question])->()){
        var questionArray = [Question]()
        
        Question.pageLabel = json["label"].stringValue
        guard let questions = json["elements"] .array else {return}
        
        for question in questions{
            let label = question["label"].stringValue
            let isMandatory = question["isMandatorky"].boolValue
            let questionType = question["type"].stringValue
            let id = question["unique_id"].stringValue
            
            let rules = question["rules"][0]
            
            let action = rules["action"].stringValue
            let target = rules["targets"][0].stringValue
            let value = rules["value"].stringValue
            let otherwise = rules["otherwise"].stringValue
            let condion = rules["condition"].stringValue
            //        let rules = question["rules"].arrayValue
            
            let question = Question(label: label, id: id, isMandatory: isMandatory, type: questionType, action: action, target: target, value: value, otherwise: otherwise, condition: condion)
            
            questionArray.append(question)
        }
        
        
        Completion(questionArray)
        
    }
    
    
    func getAppImage(imageUrl:String, completion:@escaping (_ image:UIImage?)->()){
        Alamofire.request(imageUrl).responseData { (response) in
            if response.error == nil{
                if let data = response.data{
                    completion(UIImage(data: data))
                }
            }
        }
        
        
        
    }
    
    
    
    
}
