//
//  MoreVC.swift
//  Softcom-Project
//
//  Created by Asar Sunny on 02/01/2019.
//  Copyright © 2019 Sunny. All rights reserved.
//

import UIKit
import SwiftyJSON

class MoreVC: UIViewController {

    //outlets
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var textAnswerField: UITextField!
    @IBOutlet weak var yesOrNoStack: UIStackView!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    @IBOutlet weak var submitBtn: UIButton!
    
    
    var questionArray = [Question]()
    var questionIndex = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        submitBtn.isHidden = true
        
        //get the About Questions
        DataService.instance.fetchDataFromFile { (response) in
//            self.parseData(json: response)
            let data = response["pages"][2]["sections"][0]
            
            DataService.instance.parseData(json: data, Completion: { (returnedQuestions) in
                self.questionArray = returnedQuestions
            })
        
        }
        title = Question.pageLabel
        askQuestion()
        
        // Do any additional setup after loading the view.
    }


    func askQuestion(){
        let question = questionArray[questionIndex]
        updateUI(withQuestion: question)
    }
    
    func updateUI(withQuestion question:Question){
        questionLabel.text = question.label
        switch question.type {
        case formType.text.rawValue:
            textAnswerField.isHidden = false
            yesOrNoStack.isHidden = true
        case formType.yesno.rawValue:
            yesOrNoStack.isHidden = false
        default: break

        }

    }
    
    @IBAction func nextButton(_ sender: Any) {
            questionIndex += 1
            askQuestion()
        
        if questionIndex == questionArray.count - 1{
            nextBtn.isEnabled = false
            submitBtn.isHidden = false
        }else{
           
            //do nothing
            
        }
    }
    
}
