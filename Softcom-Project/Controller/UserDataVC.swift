//
//  ViewController.swift
//  Softcom-Project
//
//  Created by Asar Sunny on 29/12/2018.
//  Copyright © 2018 Sunny. All rights reserved.
//

import UIKit
import SwiftyJSON

//var userdata = [UserData]()

class UserDataVC: UIViewController {
    var formDataArray = [UserData]()
    
    //outlets
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        
        DataService.instance.fetchDataFromFile { (response) in
                print(response)
            self.decodeData(json: response)
        }
        title = UserData.pageLabel
    }

    
    func decodeData(json:JSON){
        guard let userData = json["pages"][0]["sections"][1]["elements"].array else {return} //convert to an array
        
        UserData.pageLabel = json["pages"][0]["sections"][1]["label"].stringValue
        for element in  userData{
            let label = element["label"].stringValue
            let isMandatory = element["isMandatory"].boolValue
            let type        = element["type"].stringValue
            let rules       = element["rules"].dictionary
            let id          = element["unique_id"].stringValue
            
            let user = UserData(fieldLabel: label, isMandatory: isMandatory, fieldType: type, rules: rules, unique_id: id)
            
//            userdata.append(user)
            
            formDataArray.append(user)
    
        }
    }
    
}
  
    
extension UserDataVC:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return formDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "userdata") as? UserInfoCell{
            let field = formDataArray[indexPath.row]
            print("this is the field data \(field.fieldType)")
            
            cell.configureCell(withLabel: field.fieldLabel, dataType: field.fieldType, isMandatory: field.isMandatory, andRules: field.rules)
            
            return cell
            
        }else{
              return UITableViewCell()
        }
        
     
    }
    
    
    @IBAction func NextButton(_ sender: Any) {
    }
    
    
}

