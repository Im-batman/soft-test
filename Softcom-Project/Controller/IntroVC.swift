//
//  IntroVC.swift
//  Softcom-Project
//
//  Created by Asar Sunny on 30/12/2018.
//  Copyright © 2018 Sunny. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class IntroVC: UIViewController {

    @IBAction func UnWind(_ segue: UIStoryboardSegue){}
    
    
    //outlets
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var beginButton: UIButton!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var data:JSON?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bgImage.isHidden = true
        titleLabel.isHidden = true
        beginButton.isHidden = true
        activityIndicator.startAnimating()
        
        
        DataService.instance.fetchDataFromFile { (response) in
            self.data = response
            self.parseData(data: response)
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func parseData(data:JSON){
         let json = data["pages"][0]["sections"]
        let title = json[0]["label"].stringValue
        let imageUrl  = json[0]["elements"][0]["file"].stringValue
//        print("title-\(title)- image--\(imageUrl)")
        DispatchQueue.main.async {
         self.updateUI(withTitle: title, andImageUrl: imageUrl)
        }
        
    }
    
    
    func updateUI(withTitle title:String, andImageUrl image:String){
//        var imageR:UIImage
        titleLabel.text = title
        
        Alamofire.request(image).responseData { (response) in
            if response.error == nil{
                self.bgImage.isHidden = false
                self.titleLabel.isHidden = false
                self.beginButton.isHidden = false
                
                if let data = response.data{
                    let image = UIImage(data: data)
//                    completion(UIImage(data: data))
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                    self.bgImage.image = image
                }
            }else{
                
                
                print("failed to get image")
                self.bgImage.isHidden = false
                self.titleLabel.isHidden = false
                self.beginButton.isHidden = false
                
                self.activityIndicator.isHidden = true
                self.bgImage.image = UIImage(named:"party_girl")
                
            }
        }
        
       
        
    }
    
    
    

}
