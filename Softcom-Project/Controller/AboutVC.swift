//
//  AboutVC.swift
//  Softcom-Project
//
//  Created by Asar Sunny on 31/12/2018.
//  Copyright © 2018 Sunny. All rights reserved.
//

import UIKit
import SwiftyJSON


class AboutVC: UIViewController {

    var questionArray = [Question]()
    var questionIndex = 0
    
    
    //outlets
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var textAnswerField: UITextField!
    @IBOutlet weak var yesOrNoStack: UIStackView!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textAnswerField.isHidden = true
        yesOrNoStack.isHidden = true
        
        //get the About Questions
        DataService.instance.fetchDataFromFile { (response) in
//                self.parseData(json: response)
            let data = response["pages"][1]["sections"][0]
            DataService.instance.parseData(json: data, Completion: { (returedQuestions) in
                self.questionArray = returedQuestions
            })
            
            
        }
        title = Question.pageLabel
        askQuestion()
    }

   
    
    func askQuestion(){
    let question = questionArray[questionIndex]
        updateUI(withQuestion: question)
    }
    
    func updateUI(withQuestion question:Question){
        questionLabel.text = question.label
        
        switch question.type {
        case formType.text.rawValue:
            textAnswerField.isHidden = false
             yesOrNoStack.isHidden = true
            nextButton.isEnabled = true
        case formType.yesno.rawValue:
            yesOrNoStack.isHidden = false
            nextButton.isEnabled = false
        default: break

        }
        
    }
    
    
    //collect the answer
    @IBAction func yesOrNo(_ sender: UIButton) {
        
        //if there is no target for next question, present the next question in array

        let currentQuestion = questionArray[questionIndex]
        if currentQuestion.target == "" {
            questionIndex += 1
            askQuestion()
        }else{
             processAnswer(sender.tag)
        }
    }

    
    func processAnswer(_ tag:Int){
        var answer:String!
        if tag == 0 {
            answer = "Yes"
        }else{
            answer = "No"
        }
        
        let currentQuestion = questionArray[questionIndex]
        
        let currentQuestionAction = currentQuestion.action
        let currentQuestionValue = currentQuestion.value
        let currentQuestionOtherwise = currentQuestion.otherwise
        
        print("the values for use-- \(currentQuestionValue)---\(currentQuestionAction)--\(currentQuestionOtherwise)")
        guard let chosenAnswer = answer else {return}
        
        print("the chosen is \(chosenAnswer)")
        
        if chosenAnswer == currentQuestionValue{
            if currentQuestionAction == "show"{
                showNextTargetQuestion()
            }else{
                
                //remove the question with that target
                findTargetIndex(completion: { (index) in
                    self.questionArray.remove(at: index)
                })
                
                //if thats the last question move to next page
                if questionIndex == questionArray.count - 1{
                    performSegue(withIdentifier: "lastPageSegue", sender: nil)
                }else{
                    //otherwise add to index and show the next question
                    questionIndex += 1
                    askQuestion()
                }
                
            }
            
            
        }else{
            

            //remove the question with that target
            findTargetIndex(completion: { (index) in
                self.questionArray.remove(at: index)
            })
            
            //if thats the last question move to next page
            if questionIndex == questionArray.count - 1{
                performSegue(withIdentifier: "lastPageSegue", sender: nil)
            }else{
                //otherwise add to index and show the next question
                questionIndex += 1
                askQuestion()
            }
        
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
    
    
    func showNextTargetQuestion(){
        findTargetIndex { (index) in
            self.questionIndex = index
        }
        askQuestion()
    }
    
    
    // use the target from the current question to find the position of the target in the array
    func findTargetIndex(completion:@escaping (Int)->()){
        var index = Int()
        let targetForNextQuestion =  questionArray[questionIndex].target
        
        let questionLabels = questionArray.map({ (labels) -> String in
            return labels.label
        })
        
        for question in questionArray{
            if question.id == targetForNextQuestion{
//                let index = questionLabels.index(of: question.label)
//                questionIndex = index!
                index = questionLabels.index(of: question.label)!
            }
        }
       completion(index)
        
    }
    
    
    
    
    
    @IBAction func nextButton(_ sender: Any) {
    }
    
    
    
    
    
}
